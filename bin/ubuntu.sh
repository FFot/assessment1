#!/bin/bash

sudo apt-get update
apt -y install apache2 apache2-doc apache2-utils

cat >/var/www/html/index.html <<_END_

<html>
<body>
<marquee><h1>Hello there!</h1></marquee>
<img src="https://i.imgur.com/NtuGkUJ.jpg">
<h2>Fani</h2>
<h3>Spec:</h3>
<p>Operating system: UbuWeb</p>
<p>Web server: Apache2</p>
</body>
</html>
IP Address: $(ifconfig | grep 192 | awk '{print $2}'| sed 's/^.*://')
<p>
$(cat /proc/cpuinfo | grep 'model name')

_END_

chown root /var/www/html/index.html
chgrp root /var/www/html/index.html


service apache2 start #starts apache
service apache2 enable #make sure it starts when system does
ifconfig | grep inet | grep -v 127.0.0.1 | grep -v inet6 | grep -v 10.0.2.15
