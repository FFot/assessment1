#!/bin/bash

yum -y install haproxy
cp -f /vagrant/files/haproxy.cfg /etc/haproxy/haproxy.cfg
systemctl enable haproxy
systemctl start haproxy
ifconfig | grep inet | grep -v 127.0.0.1 | grep -v inet6 | grep -v 10.0.2.15
